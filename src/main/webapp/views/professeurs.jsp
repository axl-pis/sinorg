<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="jakarta.tags.core"%> 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Les profs</title>
</head>
<body>
	<h1>Les profs</h1>
		<ul>
		<c:forEach items="${ profs }" var="profs">
			<li>
				<a href="${ pageContext.request.contextPath }/profil/${profs.id}">${profs.firstname} ${profs.lastname}</a>
				<a href="${ pageContext.request.contextPath }/profil/${profs.id}">modifier</a>
				<a href="${ pageContext.request.contextPath }/deleteProf/${profs.id}">supprimer</a>
			</li>
		</c:forEach>
	</ul>

</body>
</html>