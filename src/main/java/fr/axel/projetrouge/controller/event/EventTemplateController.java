package fr.axel.projetrouge.controller.event;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.axel.projetrouge.model.event.EventTemplate;
import fr.axel.projetrouge.service.EventTemplateServiceImpl;
import lombok.AllArgsConstructor;

@CrossOrigin
@RestController
@RequestMapping("/forfait")
@AllArgsConstructor
public class EventTemplateController {

	//recuperation id prof et student dans token
	
	// aucun UPDATE possible
	
	private EventTemplateServiceImpl eventServiceImpl;
	
	@PostMapping
	public ResponseEntity<EventTemplate> createEventTemp(@RequestBody EventTemplate event) {
		return new ResponseEntity<EventTemplate>(this.eventServiceImpl.createEventTemplate(event), HttpStatus.CREATED);
	}
	
	@GetMapping
	public ResponseEntity<List<EventTemplate>> readAllEventTemp() {
		return new ResponseEntity<List<EventTemplate>>(this.eventServiceImpl.readAllEventTemplate(), HttpStatus.CREATED);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<EventTemplate> readEventTemp(@PathVariable Long id) {
		return new ResponseEntity<EventTemplate>(this.eventServiceImpl.readEventTemplate(id), HttpStatus.CREATED);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<EventTemplate> deleteEventTemp(@PathVariable Long id, @RequestBody EventTemplate event) {
		if (id != event.getId()) {
//			log.info("identifiant {} introuvable", id);
			return new ResponseEntity<EventTemplate>(event, HttpStatus.BAD_REQUEST);
		}
		if (this.eventServiceImpl.readEventTemplate(id) == null) {
//			log.info("identifiant {} introuvable", id);
			return new ResponseEntity<EventTemplate>(event, HttpStatus.NOT_FOUND);
		}
//		log.info("template {} supprimee", id);
		this.eventServiceImpl.updateEventTemplate(event);
		return new ResponseEntity<EventTemplate>(event, HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Boolean> deleteEventTemp(@PathVariable Long id) {
		if (this.eventServiceImpl.readEventTemplate(id) == null) {
//			log.info("identifiant {} introuvable", id);
			return new ResponseEntity<Boolean>(false, HttpStatus.NOT_FOUND);
		}
//		log.info("template {} supprimee", id);
		this.eventServiceImpl.deleteEventTemplate(id);
		return new ResponseEntity<Boolean>(true, HttpStatus.OK);
	}
	
}
