package fr.axel.projetrouge.controller.event;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.axel.projetrouge.dao.EventRepository;
import fr.axel.projetrouge.model.event.Event;
import fr.axel.projetrouge.model.event.EventTemplate;
import fr.axel.projetrouge.service.EventServiceImpl;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@AllArgsConstructor
@RequestMapping("/event")
@Slf4j
public class EventController {

	private EventServiceImpl eServiceImpl;

	
	/**************
	 * C RUDS EVENT *
	 **************/

	@PostMapping
	public Event createEvent(@RequestBody Event event) {
		return this.eServiceImpl.createEvent(event);
	}
	
//	@GetMapping("/{id}")
//	public ResponseEntity<?> readEvent(@PathVariable Long id) {
//		var event = this.eventRepository.findById(id).get();
//		if(event == null) {
//			log.error("evenement introuvable avec identifiant : {}", id);
//			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "evenement introuvable");
//		}
//		log.info("evenement d'identifiant '{}' trouve", id);
//		return new ResponseEntity<Event>(event, HttpStatus.FOUND);
//	}
	
	@GetMapping("/event")
	public String readAllEvent() {
		return "events";
	}
	
//	public ResponseEntity<?> updateEventById(@PathVariable Long id){
//		
//	}
//	
	
	
	
	
}
