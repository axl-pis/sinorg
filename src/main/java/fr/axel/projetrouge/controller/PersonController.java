package fr.axel.projetrouge.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import fr.axel.projetrouge.model.Person;
import fr.axel.projetrouge.model.Student;
import fr.axel.projetrouge.service.PersonServiceImpl;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@CrossOrigin
@RestController
@RequestMapping("/personne")
@AllArgsConstructor
@Slf4j
public class PersonController {

	private PersonServiceImpl serviceImpl;
	
	/*************
	 * CRUDS PROF *
	 *************/

	@GetMapping
	@ResponseStatus(code = HttpStatus.OK)
	public List<Person> readAllPersons() {
		return this.serviceImpl.readPersons();
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Boolean> deletePerson(@PathVariable Long id) {
		if (this.serviceImpl.searchPersonById(id) == null) {
			log.info("identifiant {} introuvable", id);
			return new ResponseEntity<Boolean>(false, HttpStatus.NOT_FOUND);
		}
		log.info("personne {} supprimee", id);
		this.serviceImpl.deletePerson(id);
		return new ResponseEntity<Boolean>(true, HttpStatus.NO_CONTENT);
	}	

	@GetMapping("/{id}")
	public ResponseEntity<Person> searchPerson(@PathVariable Long id) {
		var teacher = this.serviceImpl.searchPersonById(id);
		if (teacher == null) {
			log.error("indentifiant {} introuvable", id);
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
					"L'indentifiant ne correspond pas ou n'existe pas");
		}
		return new ResponseEntity<Person>(teacher, HttpStatus.OK);
	}

}
