package fr.axel.projetrouge.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import fr.axel.projetrouge.model.Person;
import fr.axel.projetrouge.model.Student;
import fr.axel.projetrouge.model.Teacher;
import fr.axel.projetrouge.service.PersonServiceImpl;
import fr.axel.projetrouge.service.TeacherServiceImpl;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@CrossOrigin
@RestController
@RequestMapping("/prof")
@AllArgsConstructor
@Slf4j
public class TeacherController {

	private PersonServiceImpl pServiceImpl;
	private TeacherServiceImpl tServiceImpl;

	@PostMapping
	public ResponseEntity<Teacher> creatTeacher(@RequestBody Teacher teacher) {
//		if (!this.serviceImpl.telIsUnique(teacher.getTel()))
//			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
//					"numéro de téléphone attaché à un autre compte");
//		if (!this.serviceImpl.emailIsUnique(teacher.getEmail()))
//			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "email attacher a un compte");
		log.info("teacher cree");
		return new ResponseEntity<Teacher>(this.tServiceImpl.saveTeacher(teacher), HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	public ResponseEntity<Teacher> updateTeacher(@PathVariable Long id, @RequestBody Teacher teacher) {
//		if(id != teacher.getId()) {
//			log.error("Requette inccorect {} != {}", id, teacher.getId());
//			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "identifiant ne correspond pas");
//		}
//		Person personInDb = this.serviceImpl.searchPersonById(id);
//		if(personInDb.equals(null)) {
//			log.error("eleve avec l'identifiant '{}' n'existe pas", id, teacher.getId());
//			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "identifiant ne correspond pas");
//		}
//		if(teacher.getTeacher() == null)
//			teacher.setTeacher(personInDb.getTeacher());
//		log.info("etudiant '{}' mis a jour", id);
//		return new ResponseEntity<Teacher>(this.pServiceImpl.saveStudent(teacher), HttpStatus.OK);
		return null;
	}
}
