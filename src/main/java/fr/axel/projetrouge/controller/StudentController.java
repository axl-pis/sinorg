package fr.axel.projetrouge.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import fr.axel.projetrouge.dto.StudentDTO;
import fr.axel.projetrouge.model.Student;
import fr.axel.projetrouge.model.Teacher;
import fr.axel.projetrouge.service.PersonServiceImpl;
import fr.axel.projetrouge.service.StudentServiceImpl;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@CrossOrigin
@RestController
@RequestMapping("/eleve")
@AllArgsConstructor
@Slf4j
public class StudentController {

	private PersonServiceImpl pServiceImpl;
	private StudentServiceImpl sServiceImpl;

//--------------------------------------------------------------------------------------------------------------
	/****************
	 * CRUDS STUDENT *
	 ****************/

	@PostMapping
	public ResponseEntity<Student> creatStudent(@RequestBody StudentDTO studentDTO) {
		Student student = studentDTO.getStudent();
		if (!this.pServiceImpl.telIsUnique(student.getTel()))
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "numéro de téléphone attaché à un autre compte");
		if (!this.pServiceImpl.emailIsUnique(student.getEmail()))
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "email attacher a un compte");

		var teacher = this.pServiceImpl.searchTeacherByCodeTeacher(studentDTO.getCodeTeacher());
		if (teacher == null)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "code prof incorrect");

		student.setTeacher(teacher);
		log.info("eleve cree");
		return new ResponseEntity<Student>(this.sServiceImpl.saveStudent(student), HttpStatus.CREATED);
	}

	/**
	 * MAJ user data
	 	* toute les donnees du model y compris le prof
	 	
	 	* MAJ 2.0 : changement de prof 
	 		* nouveau prof doit accepter changement
	 			* eleve acces a toutes les donnees lies a l'ancien prof mais modif impossible
 			* refuser sinon
	 * */
	@PutMapping("/{id}")
	public ResponseEntity<Student> updateStudent(@PathVariable Long id, @RequestBody StudentDTO studentDTO) {
		Student student = studentDTO.getStudent();
		if (id != student.getId()) {
			log.error("Requette inccorect {} != {}", id, student.getId());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "identifiant ne correspond pas");
		}
		
		Student studentInDb = this.sServiceImpl.readStudentById(student.getId());
		if (studentInDb.equals(null)) {
			log.error("eleve avec l'identifiant '{}' n'existe pas", id, student.getId());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "identifiant ne correspond pas");
		}
		
		Teacher newTeacher = this.pServiceImpl.searchTeacherByCodeTeacher(studentDTO.getCodeTeacher());
		if (newTeacher.equals(null)) {
			log.error("code professeur incorrect");
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "code professeur incorrect");
		}
		if(!newTeacher.equals(student.getTeacher())) {
			student.setTeacher(newTeacher);
			log.info("etudiant '{}' change d'enseignant avec {}", id, newTeacher.getId());
		}
		log.info("etudiant '{}' mis a jour avec succès", id);
		return new ResponseEntity<Student>(this.sServiceImpl.saveStudent(student), HttpStatus.OK);
	}
}
////--------------------------------------------------------------------------------------------------------------	
//
////	@PostMapping("/{id}")
////	public ResponseEntity<Student> requestNewStudentToTeacher 
////		(@RequestBody HashMap<String, String> codeTeacherStudent, @PathVariable Long id) {
////		var teacher = this.teacherRepository.findTeacherByCodeTeacher(codeTeacherStudent.get("codeTeacher"));
////		if (teacher.equals(null)) {
////			log.info("code enseignant : '{}' incorrect", codeTeacherStudent.get("codeTeacher"));
////			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "code enseignant ne correspond pas");
////		}
////
////		if(!(Integer.parseInt(codeTeacherStudent.get("idStudent")) == id)) {
////			log.info("identifiant etudiant : '{}' incorrect", codeTeacherStudent.get("idStudent"));
////			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "mauvais identifiant, recharger la page");			
////		}
////
////		var student = this.pServiceImpl.findById(id).get();
////		student.setTeacher(teacher);
////		log.info("etudiant {} ajout a l'enseignant {}", id, teacher.getId());
////        return new ResponseEntity<Student>(this.pServiceImpl.save(student), HttpStatus.OK);		
//////		return null;
////	}
//
//	//mettre un systeme qui empeche la creation multiple par mail, tel
//	//mettre un systeme de d'auto communication qu'un eleve est creer
