package fr.axel.projetrouge.controller;

import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.axel.projetrouge.dto.UserDTO;
import fr.axel.projetrouge.service.JwtService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@AllArgsConstructor
@Slf4j
public class JwtController {

	private JwtService jwtService;
	private AuthenticationManager authenticationManager;
	
	@PostMapping("/authenticate")
	public Map<String, String> createAuthenticationToken(@RequestBody UserDTO user) {
		System.out.println("authentication");
		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword())
				);
		System.out.println(authentication);
		String roles = authentication.getAuthorities().stream()
				.map(GrantedAuthority::getAuthority)
				.collect(Collectors.joining(" "));
		log.info("Utilisateur demandant le token : '{}'", authentication.getName());
		var token = this.jwtService.generateToken(authentication.getName(), roles);
		log.info("Token g ́en ́er ́e : {}", token.get("accessToken"));
		return token;
	}

}
