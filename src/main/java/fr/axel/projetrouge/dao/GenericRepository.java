package fr.axel.projetrouge.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.axel.projetrouge.model.Person;
import fr.axel.projetrouge.model.Teacher;

public interface GenericRepository extends JpaRepository<Person, Long>{
	public Optional<Teacher> findTeacherByCodeTeacher(String codeTeacher);
	public Optional<Person> findPersonByEmail(String email);
	public Optional<Person> findPersonByTel(String tel);
}
