package fr.axel.projetrouge.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.axel.projetrouge.model.event.EventTemplate;

public interface EventTemplateRepository extends JpaRepository<EventTemplate, Long> {

	public Boolean deleteEventTemplateById(Long id);
}
