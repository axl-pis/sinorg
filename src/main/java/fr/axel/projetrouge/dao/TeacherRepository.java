package fr.axel.projetrouge.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.axel.projetrouge.model.Teacher;

public interface TeacherRepository extends JpaRepository<Teacher, Long>{
}
