package fr.axel.projetrouge.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.axel.projetrouge.model.Student;

public interface StudentRepository extends JpaRepository<Student, Long>{
	public Student findStudentById(Long id);
}
