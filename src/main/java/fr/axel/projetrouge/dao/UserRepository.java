package fr.axel.projetrouge.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.axel.projetrouge.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

	public User findByEmail(String email);

}
