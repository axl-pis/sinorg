package fr.axel.projetrouge.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.axel.projetrouge.model.event.Event;

public interface EventRepository extends JpaRepository<Event, Long>{
//	public List<Event> findEventByPersonId(Long personId);
}
