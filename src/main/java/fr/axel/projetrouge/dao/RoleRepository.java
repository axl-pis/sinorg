package fr.axel.projetrouge.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.axel.projetrouge.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long>{

}
