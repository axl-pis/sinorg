package fr.axel.projetrouge.service;

import org.springframework.stereotype.Service;

import fr.axel.projetrouge.dao.TeacherRepository;
import fr.axel.projetrouge.model.Teacher;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class TeacherServiceImpl implements TeacherService {
	
	private TeacherRepository teacherRepository;

	@Override
	public Teacher saveTeacher(Teacher teacher) {
		return this.teacherRepository.save(teacher);
	}

}
