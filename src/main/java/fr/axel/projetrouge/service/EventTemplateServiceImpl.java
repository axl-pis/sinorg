package fr.axel.projetrouge.service;

import java.util.List;

import org.springframework.stereotype.Service;

import fr.axel.projetrouge.dao.EventTemplateRepository;
import fr.axel.projetrouge.model.Teacher;
import fr.axel.projetrouge.model.event.EventTemplate;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class EventTemplateServiceImpl implements EventTemplateService {

	private EventTemplateRepository evTemplateRepository;

	public EventTemplate createEventTemplate(EventTemplate event) {
		return this.evTemplateRepository.save(event);
	}
	
	public List<EventTemplate> readAllEventTemplate() {
		return this.evTemplateRepository.findAll();
	}
	
	public EventTemplate readEventTemplate(Long id) {
		return this.evTemplateRepository.findById(id).get();
	}

	public void deleteEventTemplate(Long id) {
		this.evTemplateRepository.deleteById(id);
	}

	@Override
	public List<EventTemplate> readAllEventTempByOwner(Teacher owner) {
		return null;
	}
	
	public EventTemplate updateEventTemplate(EventTemplate event) {
		return this.evTemplateRepository.save(event);
	}
}
