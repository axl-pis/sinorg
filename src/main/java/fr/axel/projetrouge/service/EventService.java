package fr.axel.projetrouge.service;

import java.util.List;

import fr.axel.projetrouge.model.event.Event;
import fr.axel.projetrouge.model.event.EventTemplate;

public interface EventService {
	public Event createEvent(Event event);
	public EventTemplate createEventTemplate(EventTemplate event);
	public Event readEventById(Long id);
	public List<Event> readEventsByIdPerson(Long idPerson);
	public Event updateEvent(Event event);
	public void deleteEvent(Long id);
}

