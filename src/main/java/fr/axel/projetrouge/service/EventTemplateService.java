package fr.axel.projetrouge.service;

import java.util.List;

import fr.axel.projetrouge.model.Teacher;
import fr.axel.projetrouge.model.event.EventTemplate;

public interface EventTemplateService {

	public List<EventTemplate> readAllEventTempByOwner(Teacher owner);
}
