package fr.axel.projetrouge.service;

import java.util.List;

import fr.axel.projetrouge.model.Person;
import fr.axel.projetrouge.model.Teacher;

public interface PersonService {
		
	public List<Person> readPersons();
			
	public void deletePerson(Long id);
	
	public Person searchPersonById(Long id);
	
	public Teacher searchTeacherByCodeTeacher(String codeTeacher);
	
	public Boolean emailIsUnique (String email);
	
	public Boolean telIsUnique (String tel);

}
