package fr.axel.projetrouge.service;

import fr.axel.projetrouge.model.Teacher;

public interface TeacherService {
	
	public Teacher saveTeacher(Teacher teacher);
			
}
