package fr.axel.projetrouge.service;

import java.util.List;

import org.springframework.stereotype.Service;

import fr.axel.projetrouge.dao.GenericRepository;
import fr.axel.projetrouge.model.Person;
import fr.axel.projetrouge.model.Teacher;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class PersonServiceImpl implements PersonService {

	private GenericRepository genericRepository;

	@Override
	public List<Person> readPersons() {
		var persons = this.genericRepository.findAll();
		if (persons.isEmpty())
			return null;
		return persons;
	}

	@Override
	public void deletePerson(Long id) {
		this.genericRepository.deleteById(id);
	}

	@Override
	public Person searchPersonById(Long id) {
		return this.genericRepository.findById(id).orElseThrow();
	}

	@Override
	public Teacher searchTeacherByCodeTeacher(String codeTeacher) {
		return this.genericRepository.findTeacherByCodeTeacher(codeTeacher).orElse(null);
	}

	@Override
	public Boolean emailIsUnique(String email) {
		return this.genericRepository.findPersonByEmail(email).isEmpty();
	}

	@Override
	public Boolean telIsUnique(String tel) {
		return this.genericRepository.findPersonByTel(tel).isEmpty();
	}
}
