package fr.axel.projetrouge.service;

import org.springframework.stereotype.Service;

import fr.axel.projetrouge.dao.StudentRepository;
import fr.axel.projetrouge.model.Student;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class StudentServiceImpl implements StudentService {

	private StudentRepository studentRepository;
	
	@Override
	public Student saveStudent(Student student) {
		return this.studentRepository.save(student);
	}

	@Override
	public Student updateStudent(Student student) {
		return this.saveStudent(student);
	}

	@Override
	public Student readStudentById(Long id) {
		return this.studentRepository.findStudentById(id);
	}
	
	
	
}
