package fr.axel.projetrouge.service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.jwt.JwtClaimsSet;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.jwt.JwtEncoderParameters;
import org.springframework.stereotype.Service;

import fr.axel.projetrouge.dto.UserDTO;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class JwtService {

	private JwtEncoder jwtEncoder;
	private JwtDecoder jwtDecoder;
	private UserDetailsServiceImpl usrDetailsServiceImpl;
	
	public Map<String, String> generateToken(String username, String roles) {
		JwtClaimsSet jwtClaims = JwtClaimsSet.builder()
				.issuedAt(Instant.now())
				.expiresAt(Instant.now().plus(1, ChronoUnit.MINUTES))
				.issuer("spring-security-oauth")
				.subject(username)
				.claim("scope", roles)
				.build();
		var token = jwtEncoder.encode(JwtEncoderParameters.from(jwtClaims)).getTokenValue();
		Map<String, String> idToken = new HashMap<>();
		idToken.put("access-token", token);
		return idToken;
	}

	public Map<String, String> generateTokens(String username, String roles) {
		JwtClaimsSet jwtClaims = JwtClaimsSet.builder()
				.issuedAt(Instant.now())
				.expiresAt(Instant.now().plus(1, ChronoUnit.DAYS))
				.issuer("spring-security-oauth")
				.subject(username)
				.build();
		var token = jwtEncoder.encode(JwtEncoderParameters.from(jwtClaims)).getTokenValue();
		var idToken = generateToken(username, roles);
		idToken.put("refresh-token", token);
		return idToken;
	}

	public Map<String, String> generateFormRefreshToken(UserDTO usr) {
		var decodedJwt = jwtDecoder.decode(usr.getRefreshToken());
		var name = decodedJwt.getSubject();
		var usrRoleConnected = usrDetailsServiceImpl.loadUserByUsername(name);
		return generateToken(name, this.getRoles(usrRoleConnected));
	}
	
	public String getRoles(UserDetails usrRoleConnected) {
		return usrRoleConnected.getAuthorities().stream()
			.map(GrantedAuthority::getAuthority) // recupere la valeur dans l'objet de type 'GrantedAuthority' avec methode de referencement
			.collect(Collectors.joining(" ")); //permet la transformation	
	}
}