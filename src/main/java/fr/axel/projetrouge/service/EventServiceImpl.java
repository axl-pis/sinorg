package fr.axel.projetrouge.service;

import java.util.List;

import org.springframework.stereotype.Service;

import fr.axel.projetrouge.dao.EventRepository;
import fr.axel.projetrouge.model.event.Event;
import fr.axel.projetrouge.model.event.EventTemplate;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class EventServiceImpl implements EventService {

	private EventRepository eventRepository;

	@Override
	public Event createEvent(Event event) {
		return this.eventRepository.save(event);
	}

	@Override
	public Event readEventById(Long id) {
 		return this.eventRepository.findById(id).orElse(null);
	}

	@Override
	public List<Event> readEventsByIdPerson(Long idPerson) {
		return null;
	}

	@Override
	public Event updateEvent(Event event) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteEvent(Long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public EventTemplate createEventTemplate(EventTemplate event) {
		// TODO Auto-generated method stub
		return null;
	}

}
