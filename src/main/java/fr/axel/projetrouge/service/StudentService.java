package fr.axel.projetrouge.service;

import fr.axel.projetrouge.model.Student;

public interface StudentService {
	
	public Student readStudentById(Long id);
	public Student updateStudent(Student student);
	public Student saveStudent(Student student);

	
}
