package fr.axel.projetrouge.dto;

import fr.axel.projetrouge.model.Student;
import lombok.Data;

@Data
public class StudentDTO {
	Student student;
	String codeTeacher;
	
}
