package fr.axel.projetrouge.dto;

import lombok.Data;

//attache les att a token pour le transfer
@Data
public class UserDTO {
	private String email;
	private String password;
	private String grantType; // att recommander par OAuth qui doit contennir soit le mot password soit le mot cles refres-token
	private String refreshToken;

}
