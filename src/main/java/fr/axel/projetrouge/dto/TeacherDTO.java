package fr.axel.projetrouge.dto;

import fr.axel.projetrouge.model.Teacher;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.NonNull;

@Data
public class TeacherDTO {
	@NonNull
	Teacher teacher;
	
	@NonNull
	@NotBlank
	String codeNewTeacher;
}
