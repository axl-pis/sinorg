package fr.axel.projetrouge.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.ToString;
import lombok.experimental.SuperBuilder;


//maper la liste d event student sur la liste d'event teacher

@Data
@SuperBuilder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "type", visible = true)
@JsonSubTypes({
	@JsonSubTypes.Type(value = Teacher.class, name = "T"),
    @JsonSubTypes.Type(value = Student.class, name = "S")
})
public abstract class Person {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@ToString.Exclude
	@EqualsAndHashCode.Include
	protected long id;

	@NonNull
	@NotBlank
	private String firstname;

	@NonNull
	@NotBlank
	private String lastname;

	@NonNull
	@Column(unique=true)
	@Email(message = "Email invalid")
	private String email;

	@NonNull
//	@Pattern(message = "Numéro de téléphone invalide", regexp = "^(?=.*[0-9])" + "(?=.*[a-z])(?=.*[A-Z])"
//			+ "(?=.*[@#$%^&+=])" + "(?=\\S+$).{8,20}$")
	private String password;

	@NonNull
	@Column(unique=true)
	// https://messente.com/documentation/number-verification/reference
	private String tel;
	
}
