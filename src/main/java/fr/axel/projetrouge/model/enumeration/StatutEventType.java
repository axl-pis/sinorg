package fr.axel.projetrouge.model.enumeration;

public enum StatutEventType {
	EVENT_ACCEPTED,
	EVENT_REFUSED,
	EVENT_REPORT,
	EVENT_COMPLETE
}
