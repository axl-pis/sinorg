package fr.axel.projetrouge.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import fr.axel.projetrouge.model.event.EventPlanned;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Table(name="student")
@Entity
@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)

public class Student extends Person {
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JsonIgnoreProperties("students")
	private Teacher teacher;
	
	@ManyToMany
	private List<EventPlanned> lessons;
	
}
