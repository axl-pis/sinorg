package fr.axel.projetrouge.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import fr.axel.projetrouge.model.event.EventPlanned;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
//@NoArgsConstructor // obligatoire car spring utilise les POJO 
//(plain old java object : obj java comme au bonn vieux temps)
@Entity
@Data
@SuperBuilder
@Table(name = "teacher")
@AllArgsConstructor
@NoArgsConstructor 
@EqualsAndHashCode(callSuper = true)

public class Teacher extends Person {

//	private List<String> paymentModeAccepted;

	@NonNull
	@NotBlank
	@Column(unique=true)
	private String codeTeacher;

	@NonNull
	@JsonIgnoreProperties("teacher")
	@OneToMany(mappedBy = "teacher")
	private List<Student> students;
	
	@OneToMany
	private List<EventPlanned> lessons;
	
	
}
