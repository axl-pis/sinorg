//package fr.axel.projetrouge.model.event;
//
//import java.sql.Date;
//import java.util.List;
//
//import jakarta.persistence.Entity;
//import jakarta.persistence.GeneratedValue;
//import jakarta.persistence.GenerationType;
//import jakarta.persistence.Id;
//import jakarta.persistence.OneToOne;
//import lombok.Data;
//import lombok.EqualsAndHashCode;
//import lombok.NoArgsConstructor;
//import lombok.ToString;
//import lombok.experimental.SuperBuilder;
//
//@Entity
//@Data
//@NoArgsConstructor
//public class OccurenceEvent {
//
//	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	@ToString.Exclude
//	@EqualsAndHashCode.Include
//	protected long id;
//	
//	@OneToOne
//	private Event event;
//	private List<Date> dates;
//}
