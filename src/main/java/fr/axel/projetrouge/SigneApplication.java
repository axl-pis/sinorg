package fr.axel.projetrouge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import fr.axel.projetrouge.securityConfig.RsaKeyProperties;

@EnableConfigurationProperties(RsaKeyProperties.class)
@SpringBootApplication
public class SigneApplication {

	public static void main(String[] args) {
		SpringApplication.run(SigneApplication.class, args);
	}
	
//	  @Bean
//	  CommandLineRunner initDatabase() {
//		  this.pServiceImpl.saveStudent(Student. ("axel", "pisani", "axel@a.fr", "pwd", "0762024242"));
//		return null;
//	  }

}
